import React, { Component} from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
    fetchDataSearch,
    handleMiniatureResult,
    fetchDataSearchDetail,
    handleMiniatureResultAll,
    handleExpandResultAll
} from '../../modules/search'
import Header from '../../components/header/header'
import SearchList from '../../components/result-list'
import SearchListMb from '../../components/result-list-mb'
import SearchDetail from '../../components/result-detail'
import './search-result.css'


class SearchResult extends Component{
    constructor(props){
        super(props);
        console.log(props)
        this.state = {
            resource: 'all',
            isDetail: false,
            isShowDropDown: false,
            isExpand : true,
            inputValue: props.location.state.value || '',
            dropdownValue: 'Best Match',
            categoryName: 'Product'
        };
        this.handleSelectResource = this.handleSelectResource.bind(this);
        this.handleShowDetail = this.handleShowDetail.bind(this);
        this.handleCollapseAll = this.handleCollapseAll.bind(this);
        this.handleShowDropDown = this.handleShowDropDown.bind(this);
        this.handleSelectCategory = this.handleSelectCategory.bind(this);
        this.onChangeInput = this.onChangeInput.bind(this);
        this.onCancelInput = this.onCancelInput.bind(this);
        this.onChangeDropdown = this.onChangeDropdown.bind(this);

        this.handleOutsideClick = this.handleOutsideClick.bind(this);
    }
    componentWillMount(){
        this.props.fetchDataSearch()
    }

    handleShowDropDown() {
        if (!this.state.isShowDropDown) {
            // attach/remove event handler
            document.addEventListener('click', this.handleOutsideClick, false);
        } else {
            document.removeEventListener('click', this.handleOutsideClick, false);
        }

        this.setState(prevState => ({
            isShowDropDown: !prevState.isShowDropDown,
        }));
    }

    handleOutsideClick(e) {
        // ignore clicks on the component itself
        if (this.node.contains(e.target)) {
            return;
        }

        this.handleShowDropDown();
    }
    handleSelectCategory(categoryName){
        this.setState({categoryName})
    }
    onChangeInput(inputValue){
        this.setState({ inputValue })
    }
    onCancelInput(){
        this.setState({ inputValue: '' })
    }
    onChangeDropdown(dropdownValue){
        this.setState({ dropdownValue })
    }
    handleCollapseAll(){
        const { isExpand } = this.state;
        const { dataSearch } = this.props;
        if(isExpand){
            this.props.handleMiniatureResultAll(dataSearch);
            this.setState({isExpand: !this.state.isExpand})
        } else {
            this.props.handleExpandResultAll(dataSearch);
            this.setState({isExpand: !this.state.isExpand})
        }
    }
    handleSelectResource(resource){
        this.setState({resource})
    }
    handleShowDetail(data){
        this.props.fetchDataSearchDetail(data);
        this.setState({isDetail: true});
    }

    render(){
        const { resource, isDetail, isShowDropDown, inputValue,dropdownValue, categoryName, isExpand } = this.state;
        return(
            <div>
                <Header isMenu={true} />
                <div className="container-search">
                    <div className="top-box-search">
                        <div className="box-form">
                            <h3>Search Result</h3>
                            <form action="">
                                <span className="icon-result icon-search" />

                                <input type="text" className="" placeholder="Keyword or Product Category" value={inputValue} defaultValue={inputValue} onChange={(e)=>{this.onChangeInput(e.target.value)}} />
                                {
                                    inputValue.length > 0 && <span className="icon-result icon-cancel" onClick={this.onCancelInput} />
                                }
                                <button>Search</button>

                                <div className="box-categories">
                                    <ul>
                                        <li className={`${categoryName === 'Product' ? 'active' : ''}`} onClick={()=>this.handleSelectCategory('Product')}>
                                            Product
                                        </li>

                                        <li className={`${categoryName === 'Notes' ? 'active' : ''}`} onClick={()=>this.handleSelectCategory('Notes')}>
                                            Notes
                                        </li>
                                        <li className={`${categoryName === 'Cloud' ? 'active' : ''}`} onClick={()=>this.handleSelectCategory('Product')}>
                                            Cloud
                                        </li>
                                        <li className={`${categoryName === 'Domino' ? 'active' : ''}`} onClick={()=>this.handleSelectCategory('Domino')}>
                                            Domino
                                        </li>
                                    </ul>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="content-box-search">
                        <div className="box-resources">
                            <ul>
                                <li className={resource === 'all' ? 'active': ''} onClick={()=>this.handleSelectResource('all')}>
                                    All Resources
                                </li>
                                <li className={resource === 'google' ? 'active': ''} onClick={()=>this.handleSelectResource('google')}>
                                    Google
                                </li>

                                <li className={resource === 'lotus' ? 'active': ''} onClick={()=>this.handleSelectResource('lotus')}>
                                    Google (Lotus.com)
                                </li>
                                <li className={resource === 'ibm' ? 'active': ''} onClick={()=>this.handleSelectResource('ibm')}>
                                    IBM Support
                                </li>
                                <li className={resource === 'forum' ? 'active': ''} onClick={()=>this.handleSelectResource('forum')}>
                                    Test Forum
                                </li>
                            </ul>
                            <div className="box-sort">
                                <div className="box-sort-content" onClick={this.handleShowDropDown} ref={node => { this.node = node; }}>
                                    <p>Sort By {dropdownValue}</p>
                                    <span className="icon-result icon-arrow"  />
                                    {

                                        isShowDropDown && <div>
                                            <div className="box-dropdown">
                                                <ul>
                                                    <li onClick={()=>{this.onChangeDropdown('Option 1')}}>
                                                        Option 1
                                                    </li>
                                                    <li onClick={()=>{this.onChangeDropdown('Option 2')}}>
                                                        Option 2
                                                    </li>
                                                </ul>

                                            </div>

                                        </div>
                                    }
                                </div>
                                {
                                    isExpand ? <a onClick={this.handleCollapseAll}>Collapse All</a>: <a onClick={this.handleCollapseAll}>Expand All</a>
                                }

                            </div>
                        </div>
                        <div className="content-box-responsive-mb">
                            <SearchListMb
                                dataSearchResult={this.props.dataSearch.filter((item)=>(item.source === resource)).length > 0 ? this.props.dataSearch.filter((item)=>(item.source === resource)) : this.props.dataSearch}
                                handleMiniature={(data, index, cb)=>this.props.handleMiniatureResult(data, index, cb)}
                                handleShowDetailData={(data)=>this.handleShowDetail(data)}
                            />
                        </div>
                        <div className="content-box-responsive-dt">
                            {
                                isDetail ? <SearchDetail
                                        dataSearchResult={this.props.dataSearch.filter((item)=>(item.source === resource)).length > 0 ? this.props.dataSearch.filter((item)=>(item.source === resource)) : this.props.dataSearch}
                                        dataDetailResult={this.props.dataDetail}
                                        handleShowDetailData={(data)=>this.handleShowDetail(data)}
                                        handleCollapseAll = {()=>{this.setState({isDetail: false});}}

                                    />
                                    :  <SearchList
                                        dataSearchResult={this.props.dataSearch.filter((item)=>(item.source === resource)).length > 0 ? this.props.dataSearch.filter((item)=>(item.source === resource)) : this.props.dataSearch}
                                        handleMiniature={(data, index, cb)=>this.props.handleMiniatureResult(data, index, cb)}
                                        handleShowDetailData={(data)=>this.handleShowDetail(data)}
                                    />
                            }
                        </div>



                    </div>

                </div>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return({
        dataSearch: state.searchActive.dataSearch,
        dataDetail: state.searchActive.dataDetail,
    })
};

const mapDispatchToProps = dispatch => bindActionCreators({
    fetchDataSearch,
    handleMiniatureResult,
    fetchDataSearchDetail,
    handleMiniatureResultAll,
    handleExpandResultAll,
    changePage: () => push('/')
}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SearchResult)
