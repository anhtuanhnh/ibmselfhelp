import React, { Component } from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
    fetchSearchResults
} from '../../modules/search'
import Header from '../../components/header/header'
import './home.css'
import img from '../../assets/imgs/multiply-nodes-39.png'
class Home extends Component{
    constructor(){
        super();
        this.state = {
            isShowSearchBox: false,
            isLoadding: false,
            isListeningVoice: false,
            isSearchingVoice: false,
            inputValue: '',
            typeSearch: '',
            dataResult: []
        }
        this.handleChoiceTypeSearch = this.handleChoiceTypeSearch.bind(this)
        this.handleListenVoice = this.handleListenVoice.bind(this)
        this.handleSearchingVoice = this.handleSearchingVoice.bind(this)
        this.onChangeInput = this.onChangeInput.bind(this)
        this.onGoSearchReult = this.onGoSearchReult.bind(this)
    }
    componentDidMount(){


    }
    onChangeInput(inputValue){
        this.props.fetchSearchResults(inputValue, (dataResult)=>{
            console.log(dataResult);
            this.setState({inputValue, dataResult});
        })
    }
    onGoSearchReult(){
        this.setState({ isLoadding: true});
        setTimeout(()=>{
            this.props.history.push({
                pathname: `/search-result`,
                state: {value: this.state.inputValue}
            })
        }, 2000)
    }
    handleChoiceTypeSearch(typeSearch){
        this.setState({typeSearch})
    }
    handleListenVoice(){
        this.setState({isListeningVoice: true})
    }
    handleSearchingVoice(){
        this.setState({isSearchingVoice: true})
        setTimeout(()=>{
            this.setState({
                isListeningVoice: false,
                inputValue: 'How to fix Cloud Server on IBM'
            });
            }, 2000
        )
    }
    render(){
        const { isLoadding, inputValue, typeSearch, isListeningVoice, isSearchingVoice, dataResult } = this.state;
        return(
           <div>
               <Header isMenu={false} />
               <div className="container-home rp-mobile">
                   <div className="box-home">
                       {
                           isLoadding
                               ? <div className="box-loadding">
                                   <span className="icon-result icon-search-white" />
                               <p>
                                   Searching...
                               </p>
                                   </div>
                               : <div>
                                   {
                                       typeSearch === 'Search' ? <form action="" className="box-form">
                                           <p className="text-hello">Hello, how can we help you ?</p>
                                           <div className="form-ctrl">
                                               <span className="icon-result icon-search" />
                                               <input type="text" className="" placeholder="Keyword or Product Category" value={inputValue} defaultValue={inputValue} onChange={(e)=>{this.onChangeInput(e.target.value)}} />
                                               <button onClick={this.onGoSearchReult}>Search</button>
                                               {
                                                   inputValue.length > 0 && <div className="box-result-search">
                                                       <ul>
                                                           {
                                                               dataResult.map((item, index)=>(
                                                                   <li key={index} onClick={this.onGoSearchReult}>
                                                                       <p>

                                                                           <b>{item.name.slice(item.name.toLowerCase().indexOf(inputValue.toLowerCase()), inputValue.length)}</b>{item.name.slice(inputValue.length, item.name.length)}
                                                                       </p>
                                                                       <div className="box-icons-result">
                                                                           {
                                                                               item.type.map((value,i)=>(
                                                                                   <div key={i} className="icon-custom">
                                                                                       <span className={`icon-result icon-${value}`} />
                                                                                       <p>{value}</p>
                                                                                   </div>
                                                                               ))
                                                                           }
                                                                       </div>
                                                                   </li>
                                                               ))
                                                           }
                                                       </ul>

                                                   </div>
                                               }
                                               <div className="box-action">
                                                   <div className="row">
                                                       <div className="col-md-4">
                                                           <span className="icon-result icon-up-right" />
                                                           <p>Enhancement</p>
                                                       </div>
                                                       <div className="col-md-4">
                                                           <span className="icon-result icon-label" />
                                                           <p>Config Storage</p>

                                                       </div>
                                                       <div className="col-md-4">
                                                           <span className="icon-result icon-cmt-md" />
                                                           <p>Forum</p>

                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </form> : <div className="box-action-step-1" >
                                               <div className="action-item item-1" onClick={()=>this.handleChoiceTypeSearch('Search')}>
                                                   <span className="icon-result icon-search-md" />
                                                   <p>Search</p>
                                               </div>
                                               <div className="action-item" >
                                                   <span className="icon-result icon-up-right" />
                                                   <p>Enhancement</p>
                                               </div>
                                               <div className="action-item">
                                                   <span className="icon-result icon-label" />
                                                   <p>Config Storage</p>

                                               </div>
                                               <div className="action-item item-4">
                                                   <span className="icon-result icon-cmt-md" />
                                                   <p>Forum</p>

                                               </div>
                                       </div>
                                   }
                               </div>
                       }
                   </div>
                   <div className="box-home-mobile">
                       {
                           isListeningVoice ? <div className="box-listening-voice">
                               <div className="listening-text">
                                   <p className="text-hello">Hello, how can we help you ?</p>
                               </div>
                               <div className="listening-status">
                                   {
                                       isSearchingVoice ? <div className="box-content-voice-search">
                                           <p>
                                               Watson is searching
                                           </p>

                                           <h3>
                                               How to fix Cloud Server on IBM
                                           </h3>
                                       </div> : <img src={img} alt=""/>


                                   }
                               </div>
                               {
                                   isSearchingVoice ? <div className="listening-action" onClick={this.handleSearchingVoice}>
                                       <p className="text-hello">CLEAR</p>
                                   </div>: <div className="listening-action" onClick={this.handleSearchingVoice}>
                                       <p className="text-hello">ABORT</p>
                                   </div>
                               }
                           </div> :<div>

                               <form className="box-form">
                                   <p className="text-hello">Hello, how can we help you ?</p>
                                   <div className="form-ctrl">
                                       <input type="text" className="" value={inputValue} defaultValue={inputValue} onChange={(e)=>{this.onChangeInput(e.target.value)}} />
                                       {
                                           inputValue.length > 0 && <div className="box-result-search">
                                               <ul>
                                                   {
                                                       dataResult.map((item, index)=>(
                                                           <li key={index} onClick={this.onGoSearchReult}>
                                                               <div className="box-icons-result">
                                                                   {
                                                                       item.type.map((value,i)=>(
                                                                           <div key={i} className="icon-custom">
                                                                               <span className={`icon-result icon-${value}`} />
                                                                               <p>{value}</p>
                                                                           </div>
                                                                       ))
                                                                   }
                                                               </div>
                                                           </li>
                                                       ))
                                                   }

                                               </ul>

                                           </div>
                                       }
                                   </div>
                                   <div className="box-mix" onClick={this.handleListenVoice}>
                                       <span className="icon-result icon-mix" />
                                   </div>

                               </form>
                           </div>
                       }
                   </div>
                   <div className=" icon-result icon-bee" id="box" />
                   <div className=" icon-result icon-bee2" id="box2" />
                   <div className=" icon-result icon-bee3" id="box3" />
                   <div className=" icon-result icon-bee4" id="box4" />
               </div>
           </div>
        )
    }
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => bindActionCreators({
    fetchSearchResults,
  changePage: () => push('/search-result-us')
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
