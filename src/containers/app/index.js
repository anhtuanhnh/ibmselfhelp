import React, { Component} from 'react';
import { Route, Link } from 'react-router-dom'
import Home from '../home'
import SearchResult from '../search-result'


import '../../assets/css/bootstrap.min.css'
import '../../assets/css/style.css'
class App extends Component {

    componentWillMount(){

    }
    render(){
        return (
            <div className="container-fluid">
                <main>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/search-result" component={SearchResult} />
                </main>
            </div>
        )
    }
}

export default App
