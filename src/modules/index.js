import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import searchActive from './search'

export default combineReducers({
  router: routerReducer,
    searchActive
})
