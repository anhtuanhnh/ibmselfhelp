import axios from 'axios'
import data from '../assets/db/db.json'
export const FETCH_DATA_SUCCESS = 'FETCH_DATA_SUCCESS'
export const FETCH_DATA_DETAIL_SUCCESS = 'FETCH_DATA_DETAIL_SUCCESS'
export const FETCH_RESULTS_SUCCESS = 'FETCH_RESULTS_SUCCESS'

const initialState = {
    dataSearch: [],
    results: [],
    dataDetail: {},
}

export default (state = initialState, action) => {
    switch (action.type) {
    case FETCH_DATA_SUCCESS:
      return {
        ...state,
          dataSearch: action.dataSearch
      }
      case FETCH_RESULTS_SUCCESS:
      return {
        ...state,
          results: action.results
      }
      case FETCH_DATA_DETAIL_SUCCESS:
      return {
        ...state,
          dataDetail: action.dataDetail
      }
    default:
      return state
  }
}

export const fetchSearchResultsSuccess = (results) => ({
    type: FETCH_RESULTS_SUCCESS,
    results
})
export const fetchDataSearchSuccess = (dataSearch) => ({
    type: FETCH_DATA_SUCCESS,
    dataSearch
})
export const fetchDataSearchDetailSuccess = (dataDetail) => ({
    type: FETCH_DATA_DETAIL_SUCCESS,
    dataDetail
})
export const fetchSearchResults = (content, cb) =>{
    return (dispatch) =>{
        const dataCatch = data.suggestions;
        const array = dataCatch.filter((item)=>{
            console.log(item.name.indexOf(content));
            return (item.name.toLowerCase().indexOf(content.toLowerCase()) !== -1)
        })
        console.log(array.length)
        if(array.length > 0){
            cb(array)
        } else {
            cb([])
        }
        // return dispatch(fetchSearchResultsSuccess(array))
    }
}
export const fetchDataSearch = () =>{
    return (dispatch) =>{
        return dispatch(fetchDataSearchSuccess(data.results))
    }
}
export const fetchDataSearchDetail = (dataDetail) =>{
    return (dispatch) =>{
        return dispatch(fetchDataSearchDetailSuccess(dataDetail))
    }
}

export const handleMiniatureResult = (dataSearch,index, cb) =>{
    return (dispatch) =>{
        const dataCatch = dataSearch;
        dataCatch[index].isMiniature = !dataCatch[index].isMiniature;
        dispatch(fetchDataSearchDetailSuccess(dataCatch));
        cb(true)
    }
}
export const handleMiniatureResultAll = dataSearch =>{
    return (dispatch) =>{
        const dataCatch = dataSearch;
        const lengthData = dataCatch.length;
        for (let i = 0; i < lengthData; i++){
            dataCatch[i].isMiniature = true;
        }
        dispatch(fetchDataSearchDetailSuccess(dataCatch))
    }
}
export const handleExpandResultAll = dataSearch =>{
    return (dispatch) =>{
        const dataCatch = dataSearch;
        const lengthData = dataCatch.length;
        for (let i = 0; i < lengthData; i++){
            dataCatch[i].isMiniature = false;
        }
        dispatch(fetchDataSearchDetailSuccess(dataCatch))
    }
}
