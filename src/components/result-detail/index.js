import React, { Component } from 'react'
import './result-detail.css'

class SearchDetail extends Component {
    constructor(props){
        super(props);
        this.state = {
        };
        this.handleShowReultDetail = this.handleShowReultDetail.bind(this)
        this.handleCollapseAll = this.handleCollapseAll.bind(this)
    }
    handleShowReultDetail(data){
        this.props.handleShowDetailData(data)
    }
    handleCollapseAll(){
        this.props.handleCollapseAll()
    }
    handleActiveCard(){
        const { dataDetailResult, dataSearchResult } = this.props;
        return dataSearchResult.indexOf(dataDetailResult);
    }
    render(){
        const { dataSearchResult, dataDetailResult } = this.props;
        console.log(dataDetailResult)
        const array = dataSearchResult || [];
        return(
            <div className="container ">
                <div className="box-result-detail">
                    <div className="list">
                        {

                            array.length > 0 && array.map((item, i)=>(
                                <div key={i} className={`card ${this.handleActiveCard(item) === i ? 'active': ''}`} >
                                    <p className="result-title" onClick={()=>{this.handleShowReultDetail(item)}}>{item.title}</p>
                                    <p className="result-text">{item.content}</p>
                                    <div className="icon-card">
                                        <span className={`icon-result icon-sub`}/>
                                        <div className="pull-right">
                                            <span className="icon-result icon-save"/>
                                            <span className="icon-result icon-move"  onClick={()=>{this.handleShowReultDetail(item)}} />
                                            <span className={`icon-result icon-${item.logo}`}/>
                                        </div>
                                    </div>
                                </div>
                            ))

                        }
                    </div>
                    <div className="detail">
                        {
                            dataDetailResult  && <div>
                                <div className="top-detail">
                                    <h3>
                                        {dataDetailResult.title}
                                    </h3>
                                    <a href="">{dataDetailResult.link}</a>
                                    <div className="top-detail-icon">
                                        <span className="icon-result icon-save"/>
                                        <span className="icon-result icon-zoom-in" onClick={this.handleCollapseAll}/>
                                    </div>
                                </div>
                                <div className="content-detail">
                                    {dataDetailResult.content}
                                </div>
                            </div>
                        }

                    </div>

                </div>

            </div>
        )
    }
}

export default SearchDetail
