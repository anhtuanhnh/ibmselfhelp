import React, { Component } from 'react'
import './result-list.css'
import MasonryInfiniteScroller   from 'react-masonry-infinite';
class SearchList extends Component {
    constructor(props){
        super(props);
        this.state = {
            hasMore: true,
            size : [{ columns: 1, gutter: 25 }, { mq: '768px', columns: 3, gutter: 25 }, { mq: '900', columns: 4, gutter: 25 }]
        };
        this.handleMiniatureResult = this.handleMiniatureResult.bind(this)
        this.handleShowReultDetail = this.handleShowReultDetail.bind(this)

    }
    componentWillReceiveProps(nextProps){
        if(nextProps.dataSearchResult && nextProps.handleMiniature){
            if(this.masonry){
               setTimeout(()=>{
                   this.masonry.forcePack()
               }, 500)
            }
        }
    }
    handleMiniatureResult(array, i){
        this.props.handleMiniature(array,i, (res)=>{
            this.forceUpdate();
            setTimeout(()=>{
                this.masonry.forcePack()
            }, 500)
        })

    }
    handleShowReultDetail(data){
        this.props.handleShowDetailData(data)
    }
    handleMoveTop(){
        window.scrollTo(500, 0);
    }
    loadMore = () => setTimeout(() => {
        console.log('loaded')
    }, 2500);
    render(){
        const { typeResource } = this.props;
        const array = this.props.dataSearchResult || [];
        return(
            <div className="container box-result-list">
                {
                    array.length > 0 && <MasonryInfiniteScroller
                        className="masonry"
                        hasMore={this.state.hasMore}
                        loader={
                            <div className="box-loading">
                                <div id="fountainG">
                                    <div id="fountainG_1" className="fountainG" />
                                    <div id="fountainG_2" className="fountainG" />
                                    <div id="fountainG_3" className="fountainG" />
                                </div>
                                <p>
                                    LOADING MORE RESULTS
                                </p>
                            </div>
                        }
                        loadMore={this.loadMore}
                        sizes = {this.state.size}
                        ref={c => this.masonry = c }
                        pack={true}
                    >
                        {
                            array.map((item, i) => {
                                if(item.isMiniature){
                                    return(
                                        <div key={i} className="card card-miniature" >
                                            <div className="icon-card">
                                                <span className={`icon-result icon-add`} onClick={()=>{this.handleMiniatureResult(array, i)}}/>
                                                <span className={`icon-result icon-${item.logo}`}/>
                                            </div>
                                            <div className="title-card">
                                                <p className="result-title" onClick={()=>this.handleShowReultDetail(item)}>{`${item.title.slice(0, 44)}...`}</p>
                                            </div>
                                        </div>
                                    )
                                } else {
                                    return(
                                        <div key={i} className="card" >
                                            <p className="result-title" onClick={()=>this.handleShowReultDetail(item)}>{`${item.title.slice(0, 44)}...`}</p>
                                            <p className="result-text">{item.content}</p>
                                            <div className="icon-card">
                                                <span className={`icon-result icon-sub`} onClick={()=>{this.handleMiniatureResult(array, i)}}/>
                                                <div className="pull-right">
                                                    <span className="icon-result icon-save"/>
                                                    <span className="icon-result icon-move" onClick={()=>this.handleShowReultDetail(item)}/>
                                                    <span className={`icon-result icon-${item.logo}`}/>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                }
                            })
                        }
                    </MasonryInfiniteScroller >

                }
                <a className="box-up-top" onClick={this.handleMoveTop}>
                    <span className="icon-result icon-up-top" />
                </a>
            </div>
        )
    }
}

export default SearchList
