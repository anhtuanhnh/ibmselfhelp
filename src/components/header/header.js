import React, {Component} from 'react'
import { Route, Link } from 'react-router-dom'

import './header.css'
class Header extends Component {
    constructor(){
        super();
        this.state = {
            isShowMenu: false
        };
        this.toggleMenu = this.toggleMenu.bind(this)
    }
    toggleMenu(){
        this.setState({isShowMenu: !this.state.isShowMenu})
    }
    render(){
        const {isMenu } = this.props;
        return(
            <header>
                <div className="box-header">
                    <Link to="/" className="logo">
                        <img src={require('../../assets/imgs/logo@3x.png')} alt="logo images"/>
                        <span>IBM</span> Self Help
                    </Link>
                    {
                        isMenu && <div className="box-menu">
                            <a>
                                <span className="icon-result icon-home" />
                                Home
                            </a>
                            <a>
                                <span className="icon-result icon-enhancement" />

                                Enhancement
                            </a>
                            <a>
                                <span className="icon-result icon-config" />
                                Config Storage
                            </a>
                            <a>
                                <span className="icon-result icon-cmt" />
                                Forum
                            </a>
                        </div>
                    }
                    <div className="pull-right">
                        <a className="btn-login">Login</a>
                    </div>
                </div>
                <div className={`header-mobile  ${this.state.isShowMenu ? 'show-menu': ''}`}>
                    <div className={`box-menu-mobile`}>
                        <a onClick={this.toggleMenu}><b>IBM</b> SELF HELP</a>
                        <span className="icon-result icon-down" />
                        <div className="box-list-menu">
                            <ul>
                                <li>
                                    Forum
                                </li>
                                <li>
                                    Enhancenments
                                </li>
                                <li>
                                    Config storage
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}

export default Header
